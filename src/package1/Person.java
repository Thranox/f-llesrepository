package package1;

public class Person implements Comparable<Person> {
	private String name, cpr;
	private int age;

	public Person(String name, String cpr, int age) {
		super();
		this.name = name;
		this.cpr = cpr;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public String getCpr() {
		return cpr;
	}

	public int getAge() {
		return age + age + age + age + age + age + age;
	}

	@Override
	public int compareTo(Person other) {
		return age - other.getAge();
	}

}
